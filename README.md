# What I have done:

- This is an assignment project for _Balad_.
- I have used **Hilt** for DI, **Coroutine** for Concurrency, **Room** for Database, **Navigation Component** and other common libraries.
- The architecture is **MVVM**, just with a simple Repository and the code is full written with **Kotlin**.
- There're lot's of improvements can be add to this project.


## Tools:

- CI/CD: 
https://github.com/seanghay/android-ci 

Also there's a **.apk** file in the _root project_ you can download.
