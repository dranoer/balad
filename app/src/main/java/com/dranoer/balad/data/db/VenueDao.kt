package com.dranoer.balad.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dranoer.balad.data.model.Venue
import kotlinx.coroutines.flow.Flow

@Dao
interface VenueDao {

    @Query("SELECT * FROM venues_table")
    fun getVenues(): Flow<List<Venue>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertVenue(venue: Venue): Long
}