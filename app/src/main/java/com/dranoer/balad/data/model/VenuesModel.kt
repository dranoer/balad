package com.dranoer.balad.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class VenuesResponse (

    @field:SerializedName("response")
    val response: VenusResponse
)

data class VenusResponse(

    @field:SerializedName("groups")
    val groups: List<Group>
)

data class Group(

    @field:SerializedName("items")
    val items: List<Item>
)

data class Item(

    @field:SerializedName("venue")
    val venue: Venue
)

@Entity(tableName = "venues_table")
data class Venue(

    @PrimaryKey
    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("name")
    val name: String? = null,
)