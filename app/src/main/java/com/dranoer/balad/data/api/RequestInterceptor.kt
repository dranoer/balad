package com.dranoer.balad.data.api

import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        // Add foursquare userless auth
        val url = chain.request()
            .url()
            .newBuilder()
            .addQueryParameter("client_id", Constant.CLIENT_ID)
            .addQueryParameter("client_secret", Constant.CLIENT_SECRET)
            .addQueryParameter("v", Constant.DATE)
            .build()

        val request = chain.request()
            .newBuilder()
            .url(url)
            .build()

        return chain.proceed(request)
    }

}