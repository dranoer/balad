package com.dranoer.balad.data.model

import com.google.gson.annotations.SerializedName

data class DetailResponse(

    @field:SerializedName("response")
    val response: DetailedItem
)

data class DetailedItem(

    @field:SerializedName("venue")
    val venue: DetailedVenue
)

data class DetailedVenue(

    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("contact")
    val contact: Contact? = null,

    @field:SerializedName("location")
    val location: LocationDetail? = null,

    @field:SerializedName("verified")
    val verified: Boolean? = null,

    @field:SerializedName("url")
    val url: String? = null,

    @field:SerializedName("rating")
    val rating: Double? = null,
)

data class Contact(
    var phone: String? = null,
    var formattedPhone: String? = null,
)

data class LocationDetail(
    var lat: Double? = null,
    var lng: Double? = null,
    var formattedAddress: List<String> = ArrayList(),
)
