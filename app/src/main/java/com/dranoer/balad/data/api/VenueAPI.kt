package com.dranoer.balad.data.api

import com.dranoer.balad.data.model.DetailResponse
import com.dranoer.balad.data.model.VenuesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface VenueAPI {

    @GET("venues/explore")
    suspend fun getAllVenues(@Query("ll") latlng: String?): Response<VenuesResponse>

    @GET("venues/{venue_id}")
    suspend fun getDetails(@Path("venue_id") venueId: String): Response<DetailResponse>
}