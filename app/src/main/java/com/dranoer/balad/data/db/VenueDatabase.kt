package com.dranoer.balad.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dranoer.balad.data.model.Venue

@Database(entities = [Venue::class], version = 1, exportSchema = false)
abstract class VenueDatabase : RoomDatabase() {

    abstract fun venueDao(): VenueDao

    companion object {
        @Volatile
        private var INSTANCE: VenueDatabase? = null

        fun getInstance(context: Context) : VenueDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        VenueDatabase::class.java,
                        "venue_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}