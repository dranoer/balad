package com.dranoer.balad.data.api

enum class Status {
    LOADING, DONE, ERROR
}