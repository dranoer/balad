package com.dranoer.balad.data.repository

import com.dranoer.balad.data.api.SafeApiRequest
import com.dranoer.balad.data.api.VenueAPI
import com.dranoer.balad.data.db.VenueDao
import com.dranoer.balad.data.model.DetailResponse
import com.dranoer.balad.data.model.Venue
import com.dranoer.balad.data.model.VenuesResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class VenueRepository @Inject constructor(
    val venueAPI: VenueAPI,
    val venueDao: VenueDao
) : SafeApiRequest() {

    val allVenues: Flow<List<Venue>> = venueDao.getVenues()

    suspend fun fetchVenues(latlng: String?): VenuesResponse {

        val response = apiRequest { venueAPI.getAllVenues(latlng) }
        for (items in response.response.groups[0].items) {
            insertVenue(items.venue)
        }

        return response
    }

    suspend fun getDetails(id: String): DetailResponse {
        return apiRequest { venueAPI.getDetails(id) }
    }

    private suspend fun insertVenue(venue: Venue) {
        venueDao.insertVenue(venue)
    }
}