package com.dranoer.balad.di

import android.content.Context
import com.dranoer.balad.data.api.Constant
import com.dranoer.balad.data.api.RequestInterceptor
import com.dranoer.balad.data.api.VenueAPI
import com.dranoer.balad.data.db.VenueDao
import com.dranoer.balad.data.db.VenueDatabase
import com.dranoer.balad.data.repository.VenueRepository
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideApiService(): VenueAPI {

        val gson = GsonBuilder().setLenient().create()

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(RequestInterceptor())
            .build()

        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(VenueAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        VenueDatabase.getInstance(appContext)

    @Singleton
    @Provides
    fun provideDao(db: VenueDatabase) = db.venueDao()


    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: VenueAPI,
        localDataSource: VenueDao
    ) =
        VenueRepository(remoteDataSource, localDataSource)

}