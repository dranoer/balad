package com.dranoer.balad.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dranoer.balad.data.model.DetailedVenue
import com.dranoer.balad.data.repository.VenueRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(var repository: VenueRepository) : ViewModel() {

    private var _venueDetail = MutableLiveData<DetailedVenue>()
    val venueDetail: LiveData<DetailedVenue> = _venueDetail

    fun getDetails(id: String) {
        viewModelScope.launch {
            try {
                _venueDetail.value = repository.getDetails(id).response.venue
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}