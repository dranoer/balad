package com.dranoer.balad.ui.main

import androidx.lifecycle.*
import com.dranoer.balad.data.api.Status
import com.dranoer.balad.data.model.Venue
import com.dranoer.balad.data.repository.VenueRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    var repository: VenueRepository
) : ViewModel() {

    val allVenues: LiveData<List<Venue>> = repository.allVenues.asLiveData()

    // Todo >> add view states and remove status
    private val _status = MutableLiveData<Status>()
    val status: LiveData<Status> = _status

    fun getVenues(latlng: String) {
        viewModelScope.launch {
            try {
                _status.value = Status.LOADING

                repository.fetchVenues(latlng)

                _status.value = Status.DONE
            } catch (e: Exception) {
                _status.value = Status.ERROR
                e.printStackTrace()
            }
        }
    }
}