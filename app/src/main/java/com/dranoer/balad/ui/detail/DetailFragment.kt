package com.dranoer.balad.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.dranoer.balad.R
import com.dranoer.balad.data.model.DetailedVenue
import com.dranoer.balad.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    val args: DetailFragmentArgs by navArgs()

    val viewModel: DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentDetailBinding.inflate(inflater, container, false)

        binding.detailViewModel = viewModel
        binding.lifecycleOwner = this

        // Make request for details
        viewModel.getDetails(args.itemId)

        // Observe venue for getting details
        viewModel.venueDetail.observe(viewLifecycleOwner) {
            initView(it)
        }

        return binding.root
    }

    private fun initView(venue: DetailedVenue) {
        if (venue.contact != null) {
            venue.contact.formattedPhone.let { phone ->
                if (phone.isNullOrEmpty()) binding.phone.visibility = View.GONE
                else binding.phone.text = resources.getString(R.string.phone, phone)

            }
        }

        when (venue.location) {
            null -> binding.address.visibility = View.GONE
            else -> {
                binding.address.text = resources.getString(
                    R.string.address,
                    venue.location.formattedAddress.toString()
                )
            }
        }

        binding.verified.text =
            if (venue.verified == true) resources.getString(R.string.verified) else resources.getString(
                R.string.not_verified
            )

        venue.url.let { site ->
            if (site.isNullOrEmpty()) binding.url.visibility = View.GONE
            else binding.url.text = resources.getString(R.string.website, site)

        }

        binding.name.text = venue.name
        binding.rating.text = resources.getString(R.string.rating, venue.rating.toString())
        binding.progressBar.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}