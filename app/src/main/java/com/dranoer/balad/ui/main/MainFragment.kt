package com.dranoer.balad.ui.main

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.dranoer.balad.R
import com.dranoer.balad.databinding.FragmentMainBinding
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    val viewModel: MainViewModel by viewModels()

    private lateinit var navHost: Fragment

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_main, container, false
        )

        navHost =
            requireActivity().supportFragmentManager.findFragmentById(R.id.nav_host_fragment)!!

        binding.mainViewModel = viewModel
        binding.lifecycleOwner = this

        val recyclerView = binding.recyclerview
        val adapter = MainAdapter(MainAdapter.OnClickListener { itemId ->
            val action = MainFragmentDirections.actionMainFragmentToDetailFragment(itemId)
            navHost.findNavController().navigate(action)
        })
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        viewModel.allVenues.observe(viewLifecycleOwner) {
            it.let {
                adapter.submitList(it)
            }
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        setUpLocationListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun setUpLocationListener() {
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity().applicationContext)
        // for getting the current location update after every 10 seconds with high accuracy
        val locationRequest = LocationRequest.create()
            .setInterval(10000).setFastestInterval(5000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        if (ActivityCompat.checkSelfPermission(
                requireActivity().applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity().applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    super.onLocationResult(locationResult)
                    for (location in locationResult.locations) {
                        val currentLocation = StringBuilder()
                        currentLocation.append(location.latitude)
                        currentLocation.append(",")
                        currentLocation.append(location.longitude)
                        Log.d("currentLocation", currentLocation.toString())

                        if (viewModel.allVenues.value!!.isEmpty()) {
                            viewModel.getVenues(currentLocation.toString())
                        }
                        binding.progressBar.visibility = View.GONE

                    }
                    // Few more things we can do here:
                    // For example: Update the location of user on server
                }
            },
            Looper.myLooper()
        )
    }

    override fun onStart() {
        super.onStart()

        when {
            PermissionUtils.isAccessFineLocationGranted(requireActivity().applicationContext) -> {
                when {
                    PermissionUtils.isLocationEnabled(requireActivity().applicationContext) -> {
                        setUpLocationListener()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(requireActivity().applicationContext)
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    activity as AppCompatActivity,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        // Todo > update permissions
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(requireActivity().applicationContext) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireActivity().applicationContext)
                        }
                    }
                } else {
                    Toast.makeText(
                        requireActivity().applicationContext,
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}